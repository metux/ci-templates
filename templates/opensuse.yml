#
# THIS FILE IS GENERATED, DO NOT EDIT
#

.fdo.opensuse:
  variables:
    FDO_DISTRIBUTION_NAME: "opensuse"
    # project 5761 is freedesktop/ci-templates
    FDO_CBUILD: https://gitlab.freedesktop.org/api/v4/projects/5761/packages/generic/cbuild/sha256-284253d93cdbb2d7c3694c50fd93d6dfe09bb63c252a7ea69927b008edfb2711/cbuild

.pull.cbuild@opensuse:
  script: &pull-cbuild
  - echo $FDO_CBUILD
  - echo $CI_API_V4_URL
  - |
    if [[ "$FDO_CBUILD" =~ ^$CI_API_V4_URL ]]
    then
      # the given URL is on the instance (gitlab.fd.o) API endpoint, use our token
      # so private projects can also retrieve cbuild in their namespace
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
           --retry 4 --fail --retry-all-errors --retry-delay 60 \
           -L -o /app/cbuild \
           $FDO_CBUILD
    else
      curl -L --retry 4 --fail --retry-all-errors --retry-delay 60 \
           -o /app/cbuild $FDO_CBUILD
    fi
  - chmod +x /app/cbuild

.fdo.container-build-common@opensuse:
  extends: .fdo.opensuse
  stage: build
  script:
  - *pull-cbuild
  - /app/cbuild --debug build-container opensuse $FDO_DISTRIBUTION_VERSION $FDO_DISTRIBUTION_TAG
  artifacts:
    paths:
      - container-build-report.xml
    reports:
      junit: container-build-report.xml

###
# Checks for a pre-existing opensuse container image and builds it if
# it does not yet exist.
#
# If an image with the same version or suffix exists in the upstream project's registry,
# the image is copied into this project's registry.
# If no such image exists, the image is built and pushed to the local registry.
#
# The architecture is native to the runner, use the `tags:` field in the job to
# select a runner that is not the default x86_64.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-opensuse-image:
#       extends: .fdo.container-build@opensuse
#       variables:
#          FDO_DISTRIBUTION_PACKAGES: 'curl wget gcc valgrind'
#          FDO_DISTRIBUTION_VERSION: 'tumbleweed'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#
#     my-opensuse-arm-image:
#       extends: .fdo.container-build@opensuse
#       variables:
#          FDO_DISTRIBUTION_PACKAGES: 'curl wget gcc valgrind'
#          FDO_DISTRIBUTION_VERSION: 'tumbleweed'
#          FDO_DISTRIBUTION_TAG: '2020-03-20-arm'
#       tags:
#         - aarch64
#
#
# **Reserved by this template:**
#
# - ``image:`` do not override
# - ``script:`` do not override
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_VERSION
#
#       **This variable is required**
#
#       The opensuse version to build, e.g. 'tumbleweed'
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
# .. attribute:: FDO_UPSTREAM_REPO
#
#       The GitLab project path to the upstream project
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       The repository name suffix to use, see below.
#
# .. attribute:: FDO_DISTRIBUTION_PACKAGES
#
#       Packages to install as a space-separated single string, e.g. "curl wget".
#       These packages must be available in the default distribution repositories; use
#       :attr:`FDO_DISTRIBUTION_EXEC` followed by the distribution-specific
#       command(s) to enable additional repositories and/or install additional
#       packages.
#
# .. attribute:: FDO_DISTRIBUTION_EXEC
#
#       An executable run after the installation of the :attr:`FDO_DISTRIBUTION_PACKAGES`
#
# .. attribute:: FDO_DISTRIBUTION_ENTRYPOINT
#
#       The path to the binary that should be used as an entrypoint
#
# .. attribute:: FDO_DISTRIBUTION_WORKINGDIR
#
#       The path that will be used as the default working directory (default: /app)
#
# .. attribute:: FDO_FORCE_REBUILD
#
#       If set, the image will be built even if it exists in the registry already
#
# .. attribute:: FDO_BASE_IMAGE
#
#       By default, the base image to start with is
#       ``opensuse:$FDO_DISTRIBUTION_VERSION``
#       and all dependencies are installed on top of that base image. If
#       ``FDO_BASE_IMAGE`` is given, it references a different base image to start with.
#       This image usually requires the full registry path, e.g.
#       ``registry.freedesktop.org/projectGroup/projectName/repo_suffix:tag-name``
#
# .. attribute:: FDO_EXPIRES_AFTER
#
#       If set, enables an expiration time on the image to
#       aid the garbage collector in deciding when an image can be removed. This
#       should be set for temporary images that are not intended to be kept
#       forever. Allowed values are e.g. ``1h`` (one hour), ``2d`` (two days) or
#       ``3w`` (three weeks).
#
# .. attribute:: FDO_CACHE_DIR
#
#       If set, the given directory is mounted as ``/cache``
#       when ``FDO_DISTRIBUTION_EXEC`` is run. This can allow for passing of
#       cache values between build jobs (if run on the same runner).  You should
#       not usually need to set this, it defaults to ``/cache`` from the host
#       and thus enables cache sharing by default.
#
# .. attribute:: FDO_USER
#
#       If set, the given unix username is used when running containers based on
#       this image instead of root. If the username is not created by the
#       :attr:`FDO_DISTRIBUTION_EXEC` script, it will be created automatically.
#       Note that $HOME will be set to ``/home/$FDO_USER``, if the user is
#       created manually by :attr:`FDO_DISTRIBUTION_EXEC` script it is important
#       to ensure that directory is created as well and writable by the user.
#
# .. attribute:: FDO_DISTRIBUTION_PLATFORM
#
#       **This variable is optional. If missing, the platform of the GitLab runner
#       will be used.**
#
#       The platform to create a container image for,  e.g. 'linux/amd64',
#       'linux/arm64/v8'. Please refer to `Go's supported platforms`_
#       and `Go's GOARM documentation <https://go.dev/wiki/GoArm>`_ for a list
#       of supported values.
#
# .. _Go's supported platforms: https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63#platform-values
#
# .. attribute:: FDO_DISTRIBUTION_COMPRESSION_FORMAT
#
#      **This variable is optional. If missing, 'gzip' will be used.**
#
#      The compression format to be used for the layers, e.g. 'gzip', 'zstd',
#      'zstd:chunked'. Selecting 'zstd:chunked' can reduce container pulling
#      time by up to 90% while also enabling deduplication of layers for
#      container engines supporting composefs (podman, buildah, ...).
#
#      Please however note that using a non-default value will prevent some
#      container engines to use the container image. This is especially true
#      for users of so-called stable distributions.
#
#      See `Podman's --compression-format documentation`_ for the list of
#      available options
#
# .. _Podman's --compression-format documentation: <https://docs.podman.io/en/latest/markdown/podman-push.1.html#compression-format-gzip-zstd-zstd-chunked>
#
# The resulting image will be pushed to the local registry.
#
# If :attr:`FDO_REPO_SUFFIX` was specified, the image path is
# ``$CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.suffixed-image@opensuse`` template to access or use this image.
#
# If :attr:`FDO_REPO_SUFFIX` was **not** specified, the image path is
# ``$CI_REGISTRY_IMAGE/opensuse/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.distribution-image@opensuse`` template to access or use this image.
#
.fdo.container-build@opensuse:
  extends: .fdo.container-build-common@opensuse
  image: quay.io/freedesktop.org/ci-templates:container-build-base-2025-01-02.0

###
# Alias to ``.fdo.container-build@opensuse``.
#
# This template is deprecated, use .fdo.container-build@opensuse instead.
# The architecture is inferred by the runner selected by the tags on the job (if any).
.fdo.container-build@opensuse@x86_64:
  extends: .fdo.container-build@opensuse


###
# Checks for a pre-existing opensuse container image for the
# ``aarch64`` processor architecture and builds it if it does not yet exist.
#
# This template requires runners with the ``aarch64`` tag.
#
# See ``.fdo.container-build@opensuse`` for details.
#
# This template is deprecated, use .fdo.container-build@opensuse instead.
# The architecture is inferred by the runner selected by the tags on the job (if any).
.fdo.container-build@opensuse@aarch64:
  extends: .fdo.container-build@opensuse
  tags:
    - aarch64


###
# opensuse template that pulls the opensuse image from the
# registry based on ``FDO_DISTRIBUTION_VERSION`` and ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@opensuse``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-opensuse-test:
#       extends: .fdo.distribution-image@opensuse
#       variables:
#          FDO_DISTRIBUTION_VERSION: 'tumbleweed'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_VERSION
#
#       **This variable is required**
#
#       The opensuse version to build, e.g. 'tumbleweed'
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@opensuse``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@opensuse``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "opensuse"
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.suffixed-image@opensuse`` instead.
#
# .. attribute:: FDO_DISTRIBUTION_PLATFORM
#
#       **This variable is optional. If missing, the platform of the GitLab runner
#       will be used.**
#
#       The platform to create a container image for,  e.g. 'linux/amd64',
#       'linux/arm64/v8'. Please refer to `Go's supported platforms`_
#       and `Go's GOARM documentation <https://go.dev/wiki/GoArm>`_ for a list
#       of supported values.
#
# .. _Go's supported platforms: https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63#platform-values
#
# .. attribute:: FDO_DISTRIBUTION_COMPRESSION_FORMAT
#
#      **This variable is optional. If missing, 'gzip' will be used.**
#
#      The compression format to be used for the layers, e.g. 'gzip', 'zstd',
#      'zstd:chunked'. Selecting 'zstd:chunked' can reduce container pulling
#      time by up to 90% while also enabling deduplication of layers for
#      container engines supporting composefs (podman, buildah, ...).
#
#      Please however note that using a non-default value will prevent some
#      container engines to use the container image. This is especially true
#      for users of so-called stable distributions.
#
#      See `Podman's --compression-format documentation`_ for the list of
#      available options
#
# .. _Podman's --compression-format documentation: <https://docs.podman.io/en/latest/markdown/podman-push.1.html#compression-format-gzip-zstd-zstd-chunked>
#
# .. attribute:: FDO_IMAGE_SIZE
#
#      Optional: resize the VM image before boot
#
#      If set, the image is resized to the given value. Uses the truncate(1)
#      commands, thus accepts suffixes like 'M' or 'G'. For example '30G'.
#
#      Note: only resizes the image file itself, not touching the content within.
#      Guest OS is responsible for resizing it's file system on boot.
.fdo.distribution-image@opensuse:
  extends: .fdo.opensuse
  image: $CI_REGISTRY_IMAGE/opensuse/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/opensuse/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG

###
# opensuse template that pulls the opensuse image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@opensuse``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-opensuse-test:
#       extends: .fdo.distribution-image@opensuse
#       variables:
#          FDO_REPO_SUFFIX: 'some/path'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
#
# **Variables:**
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       **This variable is required**
#
#       The repository name suffix.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@opensuse``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@opensuse``.
#
# .. attribute:: FDO_DISTRIBUTION_PLATFORM
#
#       **This variable is optional. If missing, the platform of the GitLab runner
#       will be used.**
#
#       The platform to create a container image for,  e.g. 'linux/amd64',
#       'linux/arm64/v8'. Please refer to `Go's supported platforms`_
#       and `Go's GOARM documentation <https://go.dev/wiki/GoArm>`_ for a list
#       of supported values.
#
# .. _Go's supported platforms: https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63#platform-values
#
# .. attribute:: FDO_DISTRIBUTION_COMPRESSION_FORMAT
#
#      **This variable is optional. If missing, 'gzip' will be used.**
#
#      The compression format to be used for the layers, e.g. 'gzip', 'zstd',
#      'zstd:chunked'. Selecting 'zstd:chunked' can reduce container pulling
#      time by up to 90% while also enabling deduplication of layers for
#      container engines supporting composefs (podman, buildah, ...).
#
#      Please however note that using a non-default value will prevent some
#      container engines to use the container image. This is especially true
#      for users of so-called stable distributions.
#
#      See `Podman's --compression-format documentation`_ for the list of
#      available options
#
# .. _Podman's --compression-format documentation: <https://docs.podman.io/en/latest/markdown/podman-push.1.html#compression-format-gzip-zstd-zstd-chunked>
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "opensuse"
#
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.distribution-image@opensuse`` instead.
.fdo.suffixed-image@opensuse:
  extends: .fdo.opensuse
  image: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
